# Team Members

* Joel Lechman
* Logan Davis
* John Bemis

# Basic SQL
SQL is a very powerful querying language used to manipulate data in large relational database management systems. We plan to show readers how to install SQL, how to create their first database, as well as how to add, modify, delete, and manipulate the data inside their own database. After showing how to install SQLite3 and explaining how to use some of the basic commands, we will prompt users with a few questions. 

*Note: For Windows users, we would like to explain how to install Cygwin, as well as apt-cyg and SQLite3. This is what I use for my queries, and I really like the workflow that Cygwin provides.*


# ER Model Diagrams
ER modeling is a visual aid to help represent a database. We would like to help readers understand entities, attributes, relationships, constraints, foreign and partial keys, weak entities, as well as identifying relationships. We want to explain what each of these subjects are, how they work, and finally show how they all cooperate in an ER model together. We will have a few questions about each subject as well as a question asking the reader to draw a complete ER model given a table. 

# Relational Algebra Operations
In order to use SQL queries, one must have a basic understanding of relational algebra and how to use the basic operations such as intersection, union, cartesian product, logical "and", as well as logical "or". We will explain what each of these operations do, how they work, and provide examples on various sets. After everything has been explained, we will provide some example questions for the reader to attempt, as well as a short seque into SQL querying. 


